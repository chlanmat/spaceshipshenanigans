﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Assets.Scripts.Utils {
    public class SpriteLine : MonoBehaviour {

        [SerializeField] private float spacing = 1f;
        [SerializeField] private Vector2 direction = Vector2.right;
        [SerializeField] private string sortingLayer = "Default";
        [SerializeField] private int sortingOrder;
        [SerializeField] private Color color = Color.white;

        private List<SpriteRenderer> sprites = new();

        public void Add(Sprite sprite) {
            GameObject go = new GameObject(sprite.name, typeof(SpriteRenderer));
            go.transform.parent = transform;
            Vector2 newPos = sprites.Count * spacing * direction.normalized;
            go.transform.localPosition = newPos;
            go.transform.localScale = Vector3.one;
            go.transform.localRotation = Quaternion.identity;

            SpriteRenderer spriteRenderer = go.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
            spriteRenderer.sortingLayerName = sortingLayer;
            spriteRenderer.sortingOrder = sortingOrder;
            spriteRenderer.color = color;

            sprites.Add(spriteRenderer);
        }

        public void Clear() {
            foreach(SpriteRenderer sr in sprites) {
                Destroy(sr.gameObject);
            }
            sprites.Clear();
        }

        public void RemoveLast() {
            Destroy(sprites[sprites.Count - 1].gameObject);
            sprites.RemoveAt(sprites.Count - 1);
        }

        public void ChangeSprite(int pos, Sprite newSprite) {
            sprites[pos].sprite = newSprite;
        }

        public int Count() {
            return sprites.Count;
        }



        private void OnValidate() {
            if (spacing < 0f) spacing = 0f;
        }
    }
}