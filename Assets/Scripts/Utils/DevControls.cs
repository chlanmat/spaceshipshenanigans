﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Utils {
    public class DevControls : MonoBehaviour {

        private void Update() {
            if (Input.GetKeyUp(KeyCode.R)) {
                Destroy(GameObject.Find("Input"));
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
            if (Input.GetKeyUp(KeyCode.Q)) {
                Application.Quit();
            }

        }
    }
}