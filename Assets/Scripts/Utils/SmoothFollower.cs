using UnityEngine;

namespace Assets.Scripts.Utils {


    /// <summary>
    /// Game object with this component will smoothly follow the target.
    /// Both this game object and target must have the same parent.
    /// </summary>

    public class SmoothFollower : MonoBehaviour {

        [SerializeField] private Transform target;
        [SerializeField] private float lerpSpeed = 10f;

        private void Start() {
            transform.localPosition = target.localPosition;
        }

        private void Update() {
            Vector3 wantedPosition = target.localPosition;
            wantedPosition.z = 0;
            transform.localPosition = Vector3.Lerp(transform.localPosition, wantedPosition, Time.deltaTime * lerpSpeed);
        }
    }
}