using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteSequence : MonoBehaviour
{

    [SerializeField] private List<Sprite> sprites;
    [SerializeField] private float frameDelay = 0.2f;
    [SerializeField, Tooltip("If true, will repead endlessly. If false, the object will " +
        "be destroyed after playback")]
        private bool repeat = false;

    private SpriteRenderer spriteRenderer;
    private Coroutine changingCoroutine;


    public void SetSprites(List<Sprite> newSprites) {
        StopCoroutine(changingCoroutine);
        sprites = newSprites;
        changingCoroutine = StartCoroutine(SpriteChanger());
    }


    private void Awake() {
        InitialChecks();
        spriteRenderer = GetComponent<SpriteRenderer>();

        changingCoroutine = StartCoroutine(SpriteChanger());
    }

    private IEnumerator SpriteChanger() {
        int spriteNum = 0;
        while (true) {
            spriteRenderer.sprite = sprites[spriteNum];
            yield return new WaitForSeconds(frameDelay);
            spriteNum++;
            if (spriteNum < sprites.Count) {
                continue;
            }
            if (repeat) {
                spriteNum = 0;
            } else {
                Destroy(gameObject);
                break;
            }
        }
    }


    private void InitialChecks() {
        if (sprites == null ||  sprites.Count == 0) {
            Debug.LogError("No sprites assigned!", gameObject);
        }
    }
}
