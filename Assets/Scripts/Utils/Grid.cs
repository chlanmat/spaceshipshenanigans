﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Utils {

    [Serializable]
    /// <summary>
    /// Represents a grid of cell positions of given size. Cell [0,0] is at bottom left.
    /// </summary>
    public struct Grid {
        public float top;
        public float right;
        public float bottom;
        public float left;
        public int cols;
        public int rows;

        public Grid(float top, float right, float bottom, float left, int cols, int rows) {
            this.top = top;
            this.right = right;
            this.bottom = bottom;
            this.left = left;
            this.cols = cols;
            this.rows = rows;
        }

        public Vector2 GetCellCenter(int col, int row) {
            return new Vector2(
                (GetVerticalBorder(col) + GetVerticalBorder(col + 1)) / 2,
                (GetHorizontalBorder(row) + GetHorizontalBorder(row + 1)) / 2
            );
        }

        public Vector2 GetCellSize() {
            return new((right - left) / cols, (top - bottom) / rows);
        }

        /// <summary>
        ///     Returns x coord of n-th left border. Counting from left.
        /// </summary>
        /// <param name="n">n-th column</param>
        public float GetVerticalBorder(int n) {
            return n * GetCellSize().x + left;
        }

        /// <summary>
        ///     Returns y coord of n-th bottom border. Counting from bottom.
        /// </summary>
        /// <param name="n">n-th row</param>
        public float GetHorizontalBorder(int n) {
            return n * GetCellSize().y + bottom;
        }



        public List<(Vector2, Vector2)> GetLines() {
            List<(Vector2, Vector2)> lines = new();
            for (int i = 0; i < cols + 1; i++) {
                lines.Add((
                    new(GetVerticalBorder(i), bottom),
                    new(GetVerticalBorder(i), top)
                ));
            }

            for (int i = 0; i < rows + 1; i++) {
                lines.Add((
                    new(left, GetHorizontalBorder(i)),
                    new(right, GetHorizontalBorder(i))
                ));
            }

            return lines;
        }
    }
}