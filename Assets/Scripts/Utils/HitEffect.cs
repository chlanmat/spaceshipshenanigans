using initi.prefabScripts;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Assets.Scripts.Utils {
    public class HitEffect : BaseHittable {

        [SerializeField] private GameObject hitMarker_pf;
        [SerializeField] private float maxMarkerTime = 0.6f;

        private List<GameObject> markers = new();
        private float lastHitTime = -1;


        private void Update() {
            if (Time.time - lastHitTime > maxMarkerTime) {
                ClearMarkers();
            }
        }

        public override void Hit(Vector2 hitPosition) {
            // Time.time is the time at the start of the frame. All hits should register in on frame.
            // Therefore if the frame changed, new scan must have been executed.
            if (Time.time != lastHitTime) { 
                ClearMarkers();
                lastHitTime = Time.time;
            }
            markers.Add(Instantiate(hitMarker_pf, hitPosition, Quaternion.identity));
        }

        private void ClearMarkers() {
            foreach (GameObject marker in markers) {
                Destroy(marker);
            }
            markers.Clear();
        }

    }
}