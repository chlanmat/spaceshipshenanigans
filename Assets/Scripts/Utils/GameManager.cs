﻿using Assets.Scripts.SceneControllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Utils {
    public class GameManager : MonoBehaviour {

        public static readonly string searchTag = "GameManager";

        public static GameManager GetInstance() {
            GameObject[] gos = GameObject.FindGameObjectsWithTag(searchTag);
            if (gos.Length == 0) {
                Debug.LogError("GameManager not found!");
                return null;
            } else if (gos.Length > 1) {
                Debug.LogError("Multiple GameManagers found!");
                return null;
            }

            GameManager manager = gos[0].GetComponent<GameManager>();
            if (manager == null) {
                Debug.LogError("GameManager not found!");
                return null;
            }
            return manager;
        }

        [SerializeField] private int maxLives = 5;
        [SerializeField] private float dangerLevelDuration = 60f;
        [SerializeField] private float gameEndDuration = 4f;
        [SerializeField] private string gameEndSceneName;
        [SerializeField] private List<DangerLevel> dangerLevels;

        private int score = 0;
        private int lives;
        private int currentDangerLevel = 0;

        /// <summary>
        /// Parameter is the new score value.
        /// </summary>
        [HideInInspector] public UnityEvent<int> scoreChangeEvent;
        /// <summary>
        /// First parameter is current lives, second parameter is max lives.
        /// </summary>
        [HideInInspector] public UnityEvent<int, int> livesChangeEvent;
        /// <summary>
        /// Invoked on every danger level change.
        /// </summary>
        [HideInInspector] public UnityEvent<DangerLevel> dangerLevelChangeEvent;
        // [HideInInspector] public UnityEvent gameStartedEvent;
        [HideInInspector] public UnityEvent gameEndedEvent;


        public void AddScore(int increment) {
            score += increment;
            scoreChangeEvent.Invoke(score);
        }

        public void ReduceLives(int decrement) {
            lives -= decrement;
            if (lives < 0) {
                lives = 0;
            }
            livesChangeEvent.Invoke(lives, maxLives);
            if (lives == 0) {
                StartCoroutine(GameEndingCoroutine());
            }
        }


        private void Awake() {
            lives = maxLives;
        }

        private void Start() {
            scoreChangeEvent.Invoke(score);
            livesChangeEvent.Invoke(lives, maxLives);
            StartCoroutine(DangerLevelIncreaser());
        }



        private IEnumerator DangerLevelIncreaser() {
            for (int i = 0; i < dangerLevels.Count; i++) {
                currentDangerLevel = i;
                dangerLevelChangeEvent.Invoke(GetDangerLevel());
                yield return new WaitForSeconds(dangerLevelDuration);
            }
        }



        public DangerLevel GetDangerLevel() {
            return dangerLevels[currentDangerLevel];
        }


        private IEnumerator GameEndingCoroutine() {
            gameEndedEvent.Invoke();
            yield return new WaitForSeconds(gameEndDuration);
            PlayerPrefs.SetInt(GameEndSceneController.playerPrefsScore, score);
            PlayerPrefs.Save();
            SceneManager.LoadScene(gameEndSceneName);
        }

    }
}