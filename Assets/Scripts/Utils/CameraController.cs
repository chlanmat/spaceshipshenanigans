﻿using Assets.Scripts.Tasks;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utils {
    public class CameraController : MonoBehaviour {

        private static CameraController _instance;
        public static CameraController Instance { get { return _instance; } }


        public readonly float defaultSize = 5f;

        private Vector2 defaultBotLeft;
        private Vector2 defaultTopRight;

        private Camera cam;



        private void Awake() {
            if (_instance != null && _instance != this) {
                Destroy(this.gameObject);
                return;
            } else {
                _instance = this;
            }

            cam = GetComponent<Camera>();
            if (cam.orthographicSize != defaultSize) {
                Debug.LogError("Camera must be set to default size!", gameObject);
            }

            defaultBotLeft = cam.ViewportToWorldPoint(Vector3.zero);
            defaultTopRight = cam.ViewportToWorldPoint(Vector3.one);

            PlayAreaSettings pas = new();
            cam.orthographicSize = pas.cameraSize.Get();
        }


        public Vector2 GetDefaultBotLeft() { return defaultBotLeft; }
        public Vector2 GetDefaultTopRight() { return defaultTopRight; }
        public Vector2 GetCurrentBotLeft() { return cam.ViewportToWorldPoint(Vector3.zero); }
        public Vector2 GetCurrentTopRight() { return cam.ViewportToWorldPoint(Vector3.one);  }

        public float GetSizeChange() { return cam.orthographicSize / defaultSize; }

        public void SetSize(float size) {
            cam.orthographicSize = size;
        }
    }
}