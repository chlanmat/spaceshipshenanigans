﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utils {
    [Serializable]
    public struct DangerLevel {

        public int level;
        public float scoreMultiplier;
        public float taskTimeMultiplier;

        public DangerLevel(int level, float scoreMultiplier, float taskTimeMultiplier) {
            this.level = level;
            this.scoreMultiplier = scoreMultiplier;
            this.taskTimeMultiplier = taskTimeMultiplier;
        }
    }
}