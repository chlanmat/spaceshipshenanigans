﻿using Assets.Scripts.Tasks;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Utils {
    public class TaskBorder : MonoBehaviour {

        [SerializeField] private Transform barStart;
        [SerializeField] private Transform barEnd;
        [SerializeField] private LineRenderer lineRenderer;
        [SerializeField] private Gradient gradient;
        [Tooltip("Position and size of this will be adjusted automatically")]
        [SerializeField] private SpriteRenderer screenBg;
        [SerializeField] private TextMeshPro successText;
        [SerializeField] private SpriteRenderer failureIndicator;

        private TaskController task;
        private SpriteRenderer spriteRenderer;

        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            successText.enabled = false;
            failureIndicator.enabled = false;
        }

        private void Start() {
            task = transform.parent.GetComponent<TaskController>();
            if (task == null ) {
                Debug.LogError("Task border must be a direct child of a Task!", gameObject);
            }

            lineRenderer.SetPositions(new Vector3[]{ barStart.localPosition, barStart.localPosition });

            screenBg.transform.localPosition = Vector3.zero;
            screenBg.size = spriteRenderer.size;
            screenBg.enabled = false;
        }

        private void Update() {
            float t = task.TimeFractionElapsed();
            lineRenderer.SetPosition(1, Vector3.Lerp(barStart.localPosition, barEnd.localPosition, t));
            SetLineColor(gradient.Evaluate(t));
        }

        public void ShowSuccess(int score) {
            screenBg.enabled = true;
            screenBg.color = Color.black;
            successText.enabled = true;
            successText.text = "Skóre:\n+ " + score;
        }

        public void ShowFailure() {
            screenBg.enabled = true;
            screenBg.color = Color.red;
            failureIndicator.enabled = true;
        }


        private void SetLineColor(Color c) {
            lineRenderer.startColor = c;
            lineRenderer.endColor = c;
        }

        [ContextMenu("Try place line ends")]
        private void TryPlaceStartEnd() {
            Vector2 max = GetComponent<SpriteRenderer>().localBounds.max;
            Vector2 min = GetComponent<SpriteRenderer>().localBounds.min;
            barStart.localPosition = new Vector2(min.x + 0.142f, min.y + 0.563f);
            barEnd.localPosition = new Vector2(min.x + 0.142f, max.y - 0.3f);
        }

        private void OnDrawGizmos() {
            if (lineRenderer != null)
                lineRenderer.SetPositions(new Vector3[] { barStart.localPosition, barEnd.localPosition });

        }

        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(barStart.position, barEnd.position);
        }

    }
}