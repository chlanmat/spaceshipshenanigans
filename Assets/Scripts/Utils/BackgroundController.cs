﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Utils {
    public class BackgroundController : MonoBehaviour {

        private Vector3 defaultScale;

        // Use this for initialization
        void Start() {
            defaultScale = transform.localScale;
        }

        // Update is called once per frame
        void Update() {
            transform.localScale = defaultScale * CameraController.Instance.GetSizeChange();
        }
    }
}