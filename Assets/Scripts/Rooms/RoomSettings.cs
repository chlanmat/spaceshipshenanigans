﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Rooms {

    [CreateAssetMenu(fileName = "Room", menuName = "ScriptableObjects/RoomSettings")]
    public class RoomSettings : ScriptableObject {

        public int touchedAnimRepeat = 1;
        public float fps = 12; 

        public List<Sprite> idle;
        public List<Sprite> touched;
    }
}