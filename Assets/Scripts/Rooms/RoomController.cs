﻿using Assets.Scripts.HitTargets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

namespace Assets.Scripts.Rooms {

    [RequireComponent(typeof(SpriteRenderer))]
    public class RoomController : MonoBehaviour {

        public static readonly Vector2 roomSize = new(2.75f, 1.638889f);

        [SerializeField] private Light2D roomLight;
        [SerializeField] private Color damagedLightColor = Color.red;
        [SerializeField] private Color destroyedLightColor = new Color(0.1f, 0.1f, 0.1f);

        public RoomSettings roomSettings;

        private bool touchedAnimIsPlaying = false;
        private bool isDamaged = false;

        private IHitTarget hitTarget;
        private SpriteRenderer spriteRenderer;
        private Coroutine playingAnimation;

        public void DamageRoom() {
            isDamaged = true;
            roomLight.color = damagedLightColor;
            hitTarget.OnPressRemoveListener(OnPress);
            if (playingAnimation != null) { StopCoroutine(playingAnimation); }
            spriteRenderer.sprite = roomSettings.idle[0];
        }

        public void DestroyRoom() {
            roomLight.color = destroyedLightColor;
        }

        public bool IsDamaged() { return isDamaged; }

        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            if (spriteRenderer == null ) { Debug.LogError("No SpriteRenderer component found!", gameObject); }
            hitTarget = GetComponent<IHitTarget>();
            if (hitTarget == null) { Debug.LogError("No IHitTarget component found!", gameObject); }
        }

        void Start() {
            hitTarget.OnPressAddListener(OnPress);

            PlayIdle();
        }


        private IEnumerator Animation(List<Sprite> sprites, int loops = int.MaxValue, bool randomStart = true) {
            float frameDelay = 1 / roomSettings.fps;
            int startingFrame = 0;
            float delayModifier = 0;
            if (randomStart) {
                startingFrame = Random.Range(0, sprites.Count);
                delayModifier = Random.Range(0f, frameDelay);
            }
            
            while (loops-- > 0) {
                for (int frame = startingFrame; frame < sprites.Count; frame++) {
                    spriteRenderer.sprite = sprites[frame];
                    yield return new WaitForSeconds(frameDelay - delayModifier);
                }

                startingFrame = 0;
                delayModifier = 0;
            }
        }

        private void PlayIdle() {
            if (playingAnimation != null) {
                StopCoroutine(playingAnimation);
            }

            if (roomSettings.idle.Count == 1) {
                spriteRenderer.sprite = roomSettings.idle[0];
            } else {
                playingAnimation = StartCoroutine(Animation(roomSettings.idle));
            }
        }

        private void OnPress(Vector2 hitPosition) {
            if (touchedAnimIsPlaying) { 
                return; 
            }

            StartCoroutine(PlayTouchedCoroutine());
        }

        private IEnumerator PlayTouchedCoroutine() {
            touchedAnimIsPlaying = true;

            if (playingAnimation != null) {
                StopCoroutine(playingAnimation);
            }

            playingAnimation = StartCoroutine(Animation(roomSettings.touched, roomSettings.touchedAnimRepeat, false));
            yield return playingAnimation;

            touchedAnimIsPlaying = false;
            PlayIdle();
        }

    }
}