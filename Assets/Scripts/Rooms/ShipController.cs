﻿using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Rooms {
    public class ShipController : MonoBehaviour {

        [SerializeField] private float shipDestroyLength = 3f;

        private List<RoomController> rooms;

        private ShipBuilder shipBuilder;


        private GameManager gameManager;

        private void Awake() {
            shipBuilder = GetComponent<ShipBuilder>();
        }

        // Use this for initialization
        void Start() {
            gameManager = GameManager.GetInstance();
            rooms = shipBuilder.BuildShip(transform);
            gameManager.livesChangeEvent.AddListener(OnLivesChange);
            gameManager.gameEndedEvent.AddListener(OnGameEnd);
        }


        private void OnLivesChange(int lives, int maxLives) {
            List<RoomController> functionalRooms = GetFunctionalRooms();

            int totalRoomsDamaged = Mathf.FloorToInt((float)(maxLives-lives) / maxLives * rooms.Count);
            int alreadyDamaged = rooms.Count - functionalRooms.Count;
            int toDamageCount = totalRoomsDamaged - alreadyDamaged;
            toDamageCount = Mathf.Min(toDamageCount, functionalRooms.Count);
            
            for (int i = 0; i < toDamageCount; i++) {
                int index = Random.Range(0, functionalRooms.Count);
                functionalRooms[index].DamageRoom();
                functionalRooms.RemoveAt(index);
            }
        }

        private void OnGameEnd() {
            StartCoroutine(DestroyRoomsCoroutine());
        }

        /// <summary>
        ///     Destroys all rooms in a random order in few seconds.
        /// </summary>
        private IEnumerator DestroyRoomsCoroutine() {

            // Shuffle rooms
            List<RoomController> roomsCopy = new(rooms);
            List<RoomController> randomOrderRooms = new();
            while (roomsCopy.Count > 0) {
                int index = Random.Range(0, roomsCopy.Count);
                randomOrderRooms.Add(roomsCopy[index]);
                roomsCopy.RemoveAt(index);
            }

            // Get random times
            List<float> destroyTimes = new();
            for (int i = 0; i < rooms.Count; i++) {
                destroyTimes.Add(Random.Range(0f, shipDestroyLength));
            }
            destroyTimes.Sort();

            // Destroy rooms
            float destructionStartTime = Time.time;
            for (int i = 0; i < randomOrderRooms.Count; i++) {
                float delay = destructionStartTime + destroyTimes[i] - Time.time;
                yield return new WaitForSeconds(delay);
                randomOrderRooms[i].DestroyRoom();
            }
        }

        /// <summary>
        ///     Returns all rooms, that have not been destroyed.
        /// </summary>
        private List<RoomController> GetFunctionalRooms() {
            List<RoomController> functionalRooms = new();
            foreach (RoomController room in rooms) {
                if (!room.IsDamaged()) {
                    functionalRooms.Add(room);
                }
            }
            return functionalRooms;
        }

    }
}