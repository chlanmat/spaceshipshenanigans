using Assets.Scripts.HitTargets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;


[RequireComponent(typeof(IHitTarget))]
public class PlaceholderRoomController : MonoBehaviour
{

    [SerializeField] private Light2D roomLight;
    [SerializeField] private float minDelay = 5f;
    [SerializeField] private float maxDelay = 20f;

    private bool ready = false;
    private IHitTarget interactiveElement;
    //private PlaceholderUI ui;

    private void Awake() {
        interactiveElement = GetComponent<IHitTarget>();
    }

    void Start()
    {
        roomLight.color = Color.red;
        interactiveElement.OnPressAddListener(OnPress);
        //ui = FindAnyObjectByType<PlaceholderUI>();
        StartCoroutine(MakeReady(Random.Range(minDelay, maxDelay)));
    }

    
    private void OnPress(Vector2 hitPosition) {
        if (ready) {
            ready = false;
            roomLight.color = Color.red;
            StartCoroutine(MakeReady(Random.Range(minDelay, maxDelay)));
            //ui.AddScore(50);
        }
    }

    private IEnumerator MakeReady(float delay) {
        yield return new WaitForSeconds(delay);
        ready = true;
        roomLight.color = Color.green;

    }

}
