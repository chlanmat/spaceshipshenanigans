﻿using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grid = Assets.Scripts.Utils.Grid;

namespace Assets.Scripts.Rooms {
    public class ShipBuilder : MonoBehaviour {

        [SerializeField] private Transform shipFront;
        [SerializeField] private Transform shipBack;
        [SerializeField] private float roomsBottomMargin = 0.9f;
        [SerializeField] private float roomsMinLRPadding = 3f;
        [SerializeField] private GameObject roomPf;
        [SerializeField] private List<RoomSettings> roomSettings;


        private Camera mainCamera;

        public List<RoomController> BuildShip(Transform parent) {
            mainCamera = Camera.main;
            Grid g = CreateRoomGrid();

            // Create rooms
            List<RoomController> rooms = new List<RoomController>();
            for (int i = 0; i < g.rows; i++) {
                for (int j = 0; j < g.cols; j++) {
                    RoomController rc = Instantiate(roomPf, g.GetCellCenter(j, i), Quaternion.identity, parent).GetComponent<RoomController>();
                    rc.roomSettings = roomSettings[Random.Range(0, roomSettings.Count)];
                    rooms.Add(rc);
                }
            }

            // Move ship front and back
            float left = g.GetVerticalBorder(0);
            float right = g.GetVerticalBorder(g.cols);
            float centerHeight = g.GetHorizontalBorder(2);
            shipFront.transform.position = new Vector3(right, centerHeight, 0);
            shipBack.transform.position = new Vector3(left, centerHeight, 0);

            return rooms;
        }

        // Returns a grid in world space
        private Grid CreateRoomGrid() {
            mainCamera = Camera.main;
            Vector2 botLeft = mainCamera.ViewportToWorldPoint(new(0, 0));
            Vector2 topRight = mainCamera.ViewportToWorldPoint(new(1, 1));

            float cameraWidth = topRight.x - botLeft.x;
            float roomsMaxWidth = cameraWidth - roomsMinLRPadding * 2f;
            int roomsColCount = Mathf.FloorToInt(roomsMaxWidth / RoomController.roomSize.x);
            float roomsWidth = roomsColCount * RoomController.roomSize.x;

            return new Grid(
                botLeft.y + roomsBottomMargin + RoomController.roomSize.y * 4,
                mainCamera.transform.position.x + roomsWidth / 2,
                botLeft.y + roomsBottomMargin,
                mainCamera.transform.position.x - roomsWidth / 2,
                roomsColCount,
                4
            );
        }






        private void OnDrawGizmos() {
            Gizmos.color = Color.white;
            Grid rect = CreateRoomGrid();

            foreach (var line in rect.GetLines()) {
                Gizmos.DrawLine(line.Item1, line.Item2);
            }
        }
    }
}