﻿using Assets.Scripts.HitTargets;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.InteractiveElements.Numeric {
    /// <summary>
    /// N*M grid. Can be used as a keypad.
    /// Requires BoxCollider2D, that defines keyboard size.
    /// </summary>

    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(IHitTarget))]
    public class InteractiveGrid : MonoBehaviour, INumericElement<int> {

        [SerializeField] private int rows = 3;
        [SerializeField] private int cols = 3;


        private int lastKey = 0;
        private UnityEvent<int, IValueElement<int>> valueChangeEvent = new();


        private BoxCollider2D boxCollider;
        private IHitTarget hitTarget;

        public void AddListenerToValueChange(UnityAction<int, IValueElement<int>> call) {
            valueChangeEvent.AddListener(call);
        }

        public void RemoveListenerFromValueChange(UnityAction<int, IValueElement<int>> call) {
            valueChangeEvent.RemoveListener(call);
        }

        public int GetValue() {
            return lastKey;
        }

        public (int min, int max) GetValueBounds() {
            return (0, (cols * rows) - 1);
        }

        public IHitTarget GetHitTarget() {
            return hitTarget;
        }


        private void Awake() {
            boxCollider = GetComponent<BoxCollider2D>();
            if (boxCollider == null) { Debug.LogError("No BoxCollider2D found!", gameObject); }
            hitTarget = GetComponent<IHitTarget>();
            if (hitTarget == null) { Debug.LogError("No IHitTarget found!", gameObject); }
        }

        private void Start() {
            hitTarget.OnPressAddListener(OnPress);
        }

        private void OnPress(Vector2 gloabalHitPos) {
            Vector2 localHitPos = transform.InverseTransformPoint(gloabalHitPos);
            int value = GetCell(localHitPos);
            lastKey = value;
            valueChangeEvent.Invoke(value, this);
        }

        /// <summary>
        /// Finds which cell is at given coordinates.
        /// </summary>
        /// <param name="localHitPos">Position in local space.</param>
        /// <returns>Cell value.</returns>
        private int GetCell(Vector2 localHitPos) {
            float cellWidth = boxCollider.size.x / cols;
            float cellHeight = boxCollider.size.y / rows;
            // Bottom left cormer of box collider is used to calculate hit position
            // as if there was origin (aka 0,0)
            Vector2 bottomLeftCorner = boxCollider.offset - boxCollider.size / 2;
            Vector2 inColliderPosition = localHitPos - bottomLeftCorner;
            int col = Mathf.Clamp(Mathf.FloorToInt(inColliderPosition.x / cellWidth), 0, cols);
            int rowFromBottom = Mathf.Clamp(Mathf.FloorToInt(inColliderPosition.y / cellHeight), 0, rows);
            int row = rows - rowFromBottom - 1;
            return row * cols + col;
        }


        private void OnDrawGizmos() {
            if (boxCollider == null) { boxCollider = GetComponent<BoxCollider2D>(); }
            if (boxCollider == null) { return; /* Component BoxCollider2D isn't attached. */ }
            Gizmos.color = Color.red;

            Vector2 botLeft = boxCollider.offset - boxCollider.size / 2;
            Vector2 topRight = boxCollider.offset + boxCollider.size / 2;
            // Draw vertical lines.
            for (int i = 0; i < cols + 1; i++) {
                float offset = i * (boxCollider.size.x / cols);
                Gizmos.DrawLine(
                    transform.TransformPoint(
                        new Vector2(botLeft.x + offset, botLeft.y)
                    ),
                    transform.TransformPoint(
                        new Vector2(botLeft.x + offset, topRight.y)
                    )
                );
            }

            // Draw horizontal lines.
            for (int i = 0; i < rows + 1; i++) {
                float offset = i * (boxCollider.size.y / rows);
                Gizmos.DrawLine(
                    transform.TransformPoint(
                        new Vector2(botLeft.x, boxCollider.size.y / 2 - offset)
                    ),
                    transform.TransformPoint(
                        new Vector2(topRight.x, boxCollider.size.y / 2 - offset)
                    )
                );
            }

        }

    }
}