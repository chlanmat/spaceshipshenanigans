﻿using Assets.Scripts.HitTargets;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.InteractiveElements.Numeric {
    public class InteractiveHoldButton : MonoBehaviour, INumericElement<float> {

        // Negative values indicate that this is not pressed
        private float pressTime = -1;

        private UnityEvent<float, IValueElement<float>> onValueChange = new();
        private IHitTarget hitTarget;

        public void AddListenerToValueChange(UnityAction<float, IValueElement<float>> call) {
            onValueChange.AddListener(call);
        }

        public void RemoveListenerFromValueChange(UnityAction<float, IValueElement<float>> call) {
            onValueChange.RemoveListener(call);
        }

        public IHitTarget GetHitTarget() {
            return hitTarget;
        }

        public float GetValue() {
            if (pressTime < 0) {
                return 0;
            }
            return Time.time - pressTime;
        }

        public (float min, float max) GetValueBounds() {
            return (0, float.MaxValue);
        }

        private void Awake() {
            hitTarget = GetComponent<IHitTarget>();
        }

        private void Start() {
            hitTarget.OnPressAddListener(OnPress);
            hitTarget.OnReleaseAddListener(OnRelease);
        }

        private void Update() {
            if (pressTime < 0) {
                return;
            }
            onValueChange.Invoke(GetValue(), this);
        }

        private void OnPress(Vector2 hitPosition) {
            pressTime = Time.time;
        }

        private void OnRelease(Vector2 hitPosition) {
            pressTime = -1;
            onValueChange.Invoke(GetValue(), this);
        }
    }
}