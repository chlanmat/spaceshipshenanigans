﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InteractiveElements.Numeric {
    public interface INumericElement<T> : IValueElement<T> {

        /// <summary>
        /// Returns lower and upper limit of the output value (inclusive).
        /// </summary>
        /// <returns>Tuple, first is minimal value, second maximal (both inclusive).</returns>
        public (T min, T max) GetValueBounds();
        
    }
}