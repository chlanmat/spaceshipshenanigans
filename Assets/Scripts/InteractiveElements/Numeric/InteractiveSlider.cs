﻿using Assets.Scripts.HitTargets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.InteractiveElements.Numeric {

    /// <summary>
    ///     Slider with a handle having a few positions.
    ///     The positions are created on a line with equal
    ///     spacing between start and end.
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(IHitTarget))]
    public class InteractiveSlider : MonoBehaviour, INumericElement<int> {

        // How many positions can the slider setttle in.
        [SerializeField] private int slots = 2;
        // GameObject representing the handle (its position is set on every hit so it will not be smooth).
        [SerializeField] private Transform handle;
        // How far from the handle position can the handle be grabbed.
        [SerializeField] private float handleLeeway = 0.3f;
        // Position of this GO is the position of first slot.
        [SerializeField] private Transform sliderStart;
        // Position of this GO is the position of last slot.
        [SerializeField] private Transform sliderEnd;
        [Tooltip("If true, value will change only after the user released the slider. " +
            "If false, the value will change on every hit")]
        [SerializeField] private bool valueOnlyOnRelease = false;
        [SerializeField] private bool randomInitialPosition = false;


        private bool held = false;
        private int value = 0;
        private UnityEvent<int, IValueElement<int>> onValueChanged = new();

        // List of slot positions. They are stored as transforms so their position is
        // updated automatically.
        private List<Transform> slotPositions = new();

        private IHitTarget hitTarget;


        public void AddListenerToValueChange(UnityAction<int, IValueElement<int>> call) {
            onValueChanged.AddListener(call);
        }

        public void RemoveListenerFromValueChange(UnityAction<int, IValueElement<int>> call) {
            onValueChanged.RemoveListener(call);
        }

        public int GetValue() {
            return value;
        }

        public (int min, int max) GetValueBounds() {
            return (0, slots - 1);
        }

        public IHitTarget GetHitTarget() {
            return hitTarget;
        }



        private void Awake() {
            hitTarget = GetComponent<IHitTarget>();

            // Create slots
            for (int i = 0; i < slots; i++) {
                Transform slot = new GameObject("Slot").transform;
                slot.parent = transform;
                slot.localPosition = Vector3.Lerp(sliderStart.localPosition, sliderEnd.localPosition, (float)i / (slots - 1));
                slotPositions.Add(slot);
            }

            // Set initial position
            int initialSlot = randomInitialPosition ? Random.Range(0, slotPositions.Count) : 0;
            handle.localPosition = slotPositions[initialSlot].localPosition;
            value = initialSlot;

        }


        void Start() {
            hitTarget.OnPressAddListener(OnPress);
            hitTarget.OnHoldAddListener(OnHold);
            hitTarget.OnReleaseAddListener(OnRelease);
        }


        private void OnPress(Vector2 hitPosition) {
            Vector3 closestPoint = GetClosestPointOnSlider(hitPosition);
            gizmo_closestPoint = closestPoint;

            if ((handle.position - closestPoint).sqrMagnitude > handleLeeway * handleLeeway) {
                return;
            }

            handle.position = closestPoint;
            SetHeld(true);
            if (!valueOnlyOnRelease) {
                int closestSlotNumber = GetClosestSlotNumber(hitPosition);
                value = closestSlotNumber;
                onValueChanged.Invoke(value, this);
            }
        }

        private void OnHold(Vector2 hitPosition) {
            if (held) {
                handle.position = GetClosestPointOnSlider(hitPosition);
                if (!valueOnlyOnRelease) {
                    int closestSlotNumber = GetClosestSlotNumber(hitPosition);
                    value = closestSlotNumber;
                    onValueChanged.Invoke(value, this);
                }
            }
        }

        private void OnRelease(Vector2 hitPosition) {
            if (held) {
                SetHeld(false);

                int closestSlotNumber = GetClosestSlotNumber(hitPosition);

                value = closestSlotNumber;
                onValueChanged.Invoke(value, this);
                handle.position = slotPositions[closestSlotNumber].position;
            }
        }

        private Vector3 GetClosestPointOnSlider(Vector3 hitPosition) {
            // Vector representation of slider path (offseted to start on [0,0])
            Vector3 path = sliderEnd.position - sliderStart.position;
            // Hit position offseted by the same amount as path
            Vector3 hitOffseted = hitPosition - sliderStart.position;
            // Find the closest point on offseted path from offseted point
            Vector3 closestPointOffseted = Vector3.Project(hitOffseted, path);

            // Clamp the point (currently the point can be anywhere on an infinite line)
            if (Mathf.Sign(closestPointOffseted.x) != Mathf.Sign(path.x)
                    || Mathf.Sign(closestPointOffseted.y) != Mathf.Sign(path.y)) {
                return slotPositions[0].position;
            }
            if (closestPointOffseted.sqrMagnitude > path.sqrMagnitude) {
                return slotPositions[slotPositions.Count - 1].position;
            }

            return closestPointOffseted + sliderStart.position;
        }

        private int GetClosestSlotNumber(Vector2 hitPosition) {
            Vector3 closestPoint = GetClosestPointOnSlider(hitPosition);
            float sqrDistanceToClosestSlot = (slotPositions[0].position - closestPoint).sqrMagnitude;
            int closestSlotNumber = 0;
            for (int i = 0; i < slots; i++) {
                float testSqrDst = (slotPositions[i].position - closestPoint).sqrMagnitude;
                if (testSqrDst < sqrDistanceToClosestSlot) {
                    sqrDistanceToClosestSlot = testSqrDst;
                    closestSlotNumber = i;
                }
            }

            return closestSlotNumber;
        }


        // Getters and setters

        private void SetHeld(bool val) {
            held = val;
        }



        Vector3 gizmo_closestPoint = new(-100000, -100000);
        private void OnDrawGizmos() {
            if (sliderStart && sliderEnd) {
                Gizmos.color = Color.yellow;
                Gizmos.DrawLine(sliderStart.position, sliderEnd.position);
                Gizmos.DrawSphere(gizmo_closestPoint, 0.1f);
            }
        }

    }
}