﻿using Assets.Scripts.HitTargets;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.InteractiveElements.Boolean {

    [RequireComponent (typeof(IHitTarget))]
    public class InteractiveButton : MonoBehaviour, IValueElement<bool> {


        private UnityEvent<bool, IValueElement<bool>> onValueChange = new();
        private bool value = false;

        IHitTarget hitTarget;

        public IHitTarget GetHitTarget() {
            return hitTarget;
        }

        public bool GetValue() {
            return value;
        }

        public void AddListenerToValueChange(UnityAction<bool, IValueElement<bool>> call) {
            onValueChange.AddListener(call);
        }

        public void RemoveListenerFromValueChange(UnityAction<bool, IValueElement<bool>> call) {
            onValueChange.RemoveListener(call);
        }

        private void Awake() {
            hitTarget = GetComponent<IHitTarget>();
        }

        private void Start() {
            hitTarget.OnPressAddListener(OnPress);
            hitTarget.OnReleaseAddListener(OnRelease);
        }


        private void OnPress(Vector2 hitPos) {
            value = true;
            onValueChange.Invoke(value, this);
        }

        private void OnRelease(Vector2 hitPos) {
            value = false;
            onValueChange.Invoke(value, this);
        }

    }
}