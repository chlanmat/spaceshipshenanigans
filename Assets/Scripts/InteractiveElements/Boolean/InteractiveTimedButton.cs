﻿using Assets.Scripts.HitTargets;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.InteractiveElements.Boolean {
    public class InteractiveTimedButton : MonoBehaviour, IValueElement<bool> {


        [SerializeField] private float pushLength = 3f;

        private float pressedTime = -1;

        private UnityEvent<bool, IValueElement<bool>> onValueChange = new();
        private bool value = false;

        IHitTarget hitTarget;

        public IHitTarget GetHitTarget() {
            return hitTarget;
        }

        public bool GetValue() {
            return value;
        }

        public void AddListenerToValueChange(UnityAction<bool, IValueElement<bool>> call) {
            onValueChange.AddListener(call);
        }

        public void RemoveListenerFromValueChange(UnityAction<bool, IValueElement<bool>> call) {
            onValueChange.RemoveListener(call);
        }

        public float GetTimeRemaining() {
            if (pressedTime < 0) {
                return float.NaN;
            } else {
                return pressedTime + pushLength - Time.time;
            }
        }

        private void Awake() {
            hitTarget = GetComponent<IHitTarget>();
        }

        private void Start() {
            hitTarget.OnPressAddListener(OnPress);
        }


        private void OnPress(Vector2 hitPos) {
            if (pressedTime >= 0) {
                return;
            }

            StartCoroutine(TimedPressCoroutine());

        }

        private IEnumerator TimedPressCoroutine() {
            pressedTime = Time.time;

            value = true;
            onValueChange.Invoke(value, this);

            Debug.Log("Button pressed");

            yield return new WaitForSeconds(pushLength);

            pressedTime = -1;

            value = false;
            onValueChange.Invoke(value, this);
        }

    }
}