﻿using Assets.Scripts.HitTargets;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.InteractiveElements {
    public interface IValueElement<T> {

        /// <summary>
        /// Set a method that will be called on every value change.
        /// </summary>
        /// <param name="call">
        /// Call that accepts two parameters. First is the new value, second is ref to the value element.
        /// </param>
        public void AddListenerToValueChange(UnityAction<T, IValueElement<T>> call);

        /// <summary>
        /// Removes listener from value change.
        /// </summary>
        /// <param name="call">
        /// Call that is listening to value change.
        /// </param>
        public void RemoveListenerFromValueChange(UnityAction<T, IValueElement<T>> call);

        /// <summary>
        /// Returns last or current value.
        /// </summary>
        /// <returns></returns>
        public T GetValue();

        /// <summary>
        /// Returns HitTarget associated with this control element.
        /// </summary>
        public IHitTarget GetHitTarget();
    }
}