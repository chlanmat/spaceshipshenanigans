using initi.input;
using initi.prefabScripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.HitTargets {

    public class SimpleHitTarget : BaseHittable, IHitTarget {
        public static readonly float maxHoldDelay = 0.3f;

        public UnityEvent<Vector2> onPress;
        public UnityEvent<Vector2> onHold;
        public UnityEvent<Vector2> onRelease;

        private bool isInteractive = true;
        private bool isHeld = false;
        private Vector2 lastHitPos = Vector2.zero;
        private float lastHitTime = -999;


        private void Start() {
            StartCoroutine(ReleaseChecker());
        }

        public override void Hit(Vector2 hitPosition) {
            if (!isInteractive) {
                return;
            }

            // Variable isNewTouch is needed. If you set lastHit(time/pos) after
            // the 'if (isNewTouch)' if statement there can be an issue. That issue
            // being calling SetInteractive(false) from onPress/onRelease/onHold.
            // If that happens, SetInteractive releases the input on previous (not
            // current) coordinates which might cause issues.
            bool isNewTouch = PastHitDelay();
            lastHitTime = Time.time;
            lastHitPos = hitPosition;

            if (isNewTouch) {
                if (!isHeld) {
                    // First hit.
                    onPress.Invoke(hitPosition);
                } else {
                    // Was already released, but ReleaseChecker coroutine didn't notice yet.
                    onRelease.Invoke(hitPosition);
                    onPress.Invoke(hitPosition);
                }
                isHeld = true;
            } else {
                onHold.Invoke(hitPosition);
            }
        }

        private IEnumerator ReleaseChecker() {
            while (true) {
                yield return new WaitForSeconds(maxHoldDelay / 2f);
                if (isHeld && PastHitDelay()) {
                    isHeld = false;
                    onRelease.Invoke(lastHitPos);
                }
            }
        }

        private bool PastHitDelay() {
            return Time.time > lastHitTime + maxHoldDelay;
        }

        public void OnPressAddListener(UnityAction<Vector2> call) {
            onPress.AddListener(call);
        }

        public void OnPressRemoveListener(UnityAction<Vector2> call) {
            onPress.RemoveListener(call);
        }

        public void OnHoldAddListener(UnityAction<Vector2> call) {
            onHold.AddListener(call);
        }

        public void OnHoldRemoveListener(UnityAction<Vector2> call) {
            onHold.RemoveListener(call);
        }

        public void OnReleaseAddListener(UnityAction<Vector2> call) {
            onRelease.AddListener(call);
        }

        public void OnReleaseRemoveListener(UnityAction<Vector2> call) {
            onRelease.RemoveListener(call);
        }

        public void SetInteractive(bool interactive) {
            if (isInteractive == interactive) {
                return;
            }

            isInteractive = interactive;
            if (!isInteractive && isHeld) {
                isHeld = false;
                onRelease.Invoke(lastHitPos);
            }
        }

        public bool IsInteractive() {
            return isInteractive;
        }
    }
}