﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.HitTargets {
    public interface IHitTarget {


        public void SetInteractive(bool interactive);
        public bool IsInteractive();
        public void OnPressAddListener(UnityAction<Vector2> call);
        public void OnPressRemoveListener(UnityAction<Vector2> call);
        public void OnHoldAddListener(UnityAction<Vector2> call);
        public void OnHoldRemoveListener(UnityAction<Vector2> call);
        public void OnReleaseAddListener(UnityAction<Vector2> call);
        public void OnReleaseRemoveListener(UnityAction<Vector2> call);

    }
}