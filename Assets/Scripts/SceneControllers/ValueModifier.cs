﻿using Assets.Scripts.UI;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.SceneControllers {
    public class ValueModifier : MonoBehaviour {

        [SerializeField] private SettingsSceneController.PlayAreaVars variable;
        [SerializeField] private float step;
        [SerializeField] private int decimalPlaces = 2;
        [SerializeField] private TextMeshPro text;
        [SerializeField] private GameObject increaseGO;
        [SerializeField] private GameObject decreaseGO;
        private IUiActionElement increase;
        private IUiActionElement decrease;
        private float value;
        private SettingsSceneController settingsSceneController;
        private string format;

        private void Start() {
            format = "0.";
            for (int i = 0; i < decimalPlaces; i++) { format += "0"; }

            increase = increaseGO.GetComponent<IUiActionElement>();
            increase.AddListenerToAction(OnIncrease);
            decrease = decreaseGO.GetComponent<IUiActionElement>();
            decrease.AddListenerToAction(OnDecrease);
            settingsSceneController = FindAnyObjectByType<SettingsSceneController>();
            value = settingsSceneController.Get(variable);
            text.text = value.ToString(format);
        }

        private void OnIncrease() {
            ChangeValue(step);
        }

        private void OnDecrease() { 
            ChangeValue(-step);
        }

        private void ChangeValue(float change) {
            value += change;
            text.text = value.ToString(format);
            settingsSceneController.Set(variable, value);
        }

    }
}