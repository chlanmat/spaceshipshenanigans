﻿using Assets.Scripts.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.SceneControllers {
    public class GameEndSceneController : MonoBehaviour {

        public static readonly string playerPrefsScore = "score";

        [SerializeField] private GameObject keyboardPf;
        [SerializeField] private List<UiHoldButton> buttons;
        [SerializeField] private ScoreboardController scoreboard;

        private int playerScore;
        ScreenKeyboardController keyboardInstance;

        private void Start() {
            playerScore = GetScore();
            if (scoreboard.FindScoreboardPos(playerScore) != -1) {
                // Team scored new high score
                SetButtonsInteractive(false);
                keyboardInstance = Instantiate(keyboardPf).GetComponent<ScreenKeyboardController>();
                keyboardInstance.returnName.AddListener(SetTeamName);
            } else {
                // Team did not place on scoreboard
                scoreboard.ShowScoreboard();
            }
        }

        /// <returns>Score from PlayerPrefs.</returns>
        private int GetScore() {
            if (!PlayerPrefs.HasKey(playerPrefsScore)) {
                Debug.LogError("No score in PlayerPrefs");
                return -1;
            }
            return PlayerPrefs.GetInt(playerPrefsScore);
        }

        public void SetTeamName(string teamName) {
            scoreboard.AddEntry(teamName, playerScore);
            scoreboard.ShowScoreboard();
            Destroy(keyboardInstance.gameObject);
            SetButtonsInteractive(true);
        }

        private void SetButtonsInteractive(bool interactive) {
            foreach (var button in buttons) {
                button.SetButtonInteractive(interactive);
            }
        }

    }
}