﻿using Assets.Scripts.Tasks;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;
using Grid = Assets.Scripts.Utils.Grid;

namespace Assets.Scripts.SceneControllers {
    public class SettingsSceneController : MonoBehaviour {

        PlayAreaSettings playArea;
        LineRenderer lineRenderer;
        Camera mainCamera;

        private void Awake() {
            lineRenderer = GetComponent<LineRenderer>();
            playArea = new();
            mainCamera = Camera.main;
        }

        private void Start() {
            DrawGrid();
        }

        private void OnDisable() {
            playArea.Save();
        }

        public void Set(PlayAreaVars variable, float value) {
            switch (variable) {
                case PlayAreaVars.leftPadding:
                    playArea.leftPadding.Set(value);
                    break;
                case PlayAreaVars.rightPadding:
                    playArea.rightPadding.Set(value);
                    break;
                case PlayAreaVars.bottomPadding:
                    playArea.bottomPadding.Set(value);
                    break;
                case PlayAreaVars.topPadding:
                    playArea.topPadding.Set(value);
                    break;
                case PlayAreaVars.cameraSize:
                    playArea.cameraSize.Set(value);
                    CameraController.Instance.SetSize(playArea.cameraSize.Get());
                    break;
                case PlayAreaVars.touchRows:
                    playArea.touchRows.Set(Mathf.RoundToInt(value));
                    break;
            }
            DrawGrid();
        }

        public float Get(PlayAreaVars variable) {
            switch (variable) {
                case PlayAreaVars.leftPadding:
                    return playArea.leftPadding.Get();
                case PlayAreaVars.rightPadding:
                    return playArea.rightPadding.Get();
                case PlayAreaVars.bottomPadding:
                    return playArea.bottomPadding.Get();
                case PlayAreaVars.topPadding:
                    return playArea.topPadding.Get();
                case PlayAreaVars.cameraSize:
                    return playArea.cameraSize.Get();
                case PlayAreaVars.touchRows:
                    return playArea.touchRows.Get();
                default:
                    return -9999999;
            }
        }

        private void DrawGrid() {
            
            Vector2 botLeft = mainCamera.ViewportToWorldPoint(new(0, 0));
            Vector2 topRight = mainCamera.ViewportToWorldPoint(new(1, 1));

            float top = Mathf.Lerp(topRight.y, botLeft.y, playArea.topPadding.Get());
            float right = Mathf.Lerp(topRight.x, botLeft.x, playArea.rightPadding.Get());
            float bottom = Mathf.Lerp(botLeft.y, topRight.y, playArea.bottomPadding.Get());
            float left = Mathf.Lerp(botLeft.x, topRight.x, playArea.leftPadding.Get());

            Grid g = new Grid(
                top,
                right,
                bottom,
                left,
                Mathf.FloorToInt((right - left) / TaskController.maxTaskSize.x),
                Mathf.FloorToInt((top - bottom) / TaskController.maxTaskSize.y)
            );

            List<Vector3> positions = new();
            foreach (var line in g.GetLines()) {
                positions.Add(line.Item1);
                positions.Add(line.Item2);
                positions.Add(line.Item1);
            }

            lineRenderer.positionCount = positions.Count;
            lineRenderer.SetPositions(positions.ToArray());

        }


        public enum PlayAreaVars { leftPadding,  rightPadding, bottomPadding, topPadding, touchRows, cameraSize }
    }
}