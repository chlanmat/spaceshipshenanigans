﻿using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks {

    /// <summary>
    /// This class handles the common task actions like keeping time,
    /// adding score or removing lives. 
    /// It works alongside specific task script.
    /// It has to be at the top object in task prefab.
    /// </summary>
    public class TaskController : MonoBehaviour {

        public static readonly Vector2 maxTaskSize = new(5.3f, 2.6f);
        
        private static readonly float easeInOutTime = 0.2f;

        [SerializeField] private int scoreAward = 100;
        [SerializeField] private float defaultLength = 20f;

        private float length;
        // Negative values indicate StartTask has not been called.
        private float startTime = -1;
        // Negative values indicate task has not ended yet.
        private float endTime = -1;

        private TaskBorder border;

        private GameManager gameManager;


        // Called from task spawner.
        public void StartTask(float timeMultiplier) {
            length = defaultLength * timeMultiplier;
            startTime = Time.time;
            StartCoroutine(ScaleChanger(Vector3.zero, Vector3.one, easeInOutTime));
            StartCoroutine(TimeoutChecker(length));
        }

        /// <summary>
        /// Ends the timer and destroys the task after a delay (can be 0).
        /// </summary>
        public void EndTask(bool success, float delay = 0) {
            if (HasEnded()) {
                return;
            }

            endTime = Time.time;
            StartCoroutine(EndingCoroutine(success, delay));
        }

        /// <summary>
        /// Finds how much time has passed proportionally to length. 
        /// 0: task just started, 1: task finished.
        /// </summary>
        /// <returns></returns>
        public float TimeFractionElapsed() {
            if (!HasStarted()) {
                return 0;
            }
            if (HasEnded()) {
                return (endTime - startTime) / length;
            } else {
                return (Time.time - startTime) / length;
            }
        }

        private void Start() {
            gameManager = GameManager.GetInstance();
            border = GetComponentInChildren<TaskBorder>();
        }

        private IEnumerator EndingCoroutine(bool success, float delay) {
            yield return new WaitForSeconds(delay);

            if (success) {
                int increment = Mathf.RoundToInt(scoreAward * gameManager.GetDangerLevel().scoreMultiplier);
                gameManager.AddScore(increment);
                border.ShowSuccess(increment);
            } else {
                gameManager.ReduceLives(1);
                border.ShowFailure();
            }

            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(ScaleChanger(Vector3.one, Vector3.zero, easeInOutTime));
            Destroy(gameObject);
        }


        private IEnumerator ScaleChanger(Vector3 start, Vector3 end, float duration) {
            transform.localScale = start;
            float startTime = Time.time;
            while (Time.time < startTime + duration) {
                float ratio = (Time.time - startTime) / duration;
                transform.localScale = Vector3.Lerp(start, end, ratio);
                yield return null;
            }
            transform.localScale = end;
        }

        private IEnumerator TimeoutChecker(float duration) {
            yield return new WaitForSeconds(duration);
            if (!HasEnded()) {
                EndTask(false);
            }
        }


        public bool HasEnded() {
            return endTime >= 0;
        }

        public bool HasStarted() {
            return startTime >= 0;
        }

        public float GetLength() {
            return length;
        }

        private void OnDrawGizmos() {
            Gizmos.color = Color.white;
            Gizmos.DrawWireCube(transform.position, maxTaskSize);
        }
    }
}