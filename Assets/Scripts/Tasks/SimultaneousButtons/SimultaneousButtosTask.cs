﻿using Assets.Scripts.InteractiveElements;
using Assets.Scripts.InteractiveElements.Boolean;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Tasks.SimultaneousButtons {

    [RequireComponent(typeof(TaskController))]
    public class SimultaneousButtosTask : MonoBehaviour {

        [SerializeField] private InteractiveTimedButton leftButton;
        [SerializeField] private InteractiveTimedButton rightButton;

        public UnityEvent taskCompletedEvent;

        private TaskController controller;


        private void Awake() {
            controller = GetComponent<TaskController>();
        }

        private void Start() {
            leftButton.AddListenerToValueChange(OnValueChange);
            rightButton.AddListenerToValueChange(OnValueChange);
        }

        private void OnValueChange(bool value, IValueElement<bool> element) {
            if (leftButton.GetValue() && rightButton.GetValue()) {
                controller.EndTask(true, 1f);
                taskCompletedEvent.Invoke();
            }
        }
    }
}