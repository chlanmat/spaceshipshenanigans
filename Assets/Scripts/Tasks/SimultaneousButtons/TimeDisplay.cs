﻿using Assets.Scripts.InteractiveElements.Boolean;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Tasks.SimultaneousButtons {
    public class TimeDisplay : MonoBehaviour {

        [SerializeField] private InteractiveTimedButton button;

        private TextMeshPro text;

        private void Awake() {
            text = GetComponent<TextMeshPro>();
        }

        private void Update() {
            float timeRemaining = button.GetTimeRemaining();
            if (float.IsNaN(timeRemaining) || Mathf.CeilToInt(timeRemaining) == 0) {
                text.text = string.Empty;
            } else {
                text.text = Mathf.CeilToInt(timeRemaining).ToString();
            }
        }
    }
}