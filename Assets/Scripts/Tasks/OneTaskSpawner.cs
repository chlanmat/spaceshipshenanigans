﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks {
    public class OneTaskSpawner : MonoBehaviour {

        [SerializeField] private GameObject task_pf;

        private GameObject taskInstance;

        private void Update() {
            if (taskInstance == null) {
                taskInstance = Instantiate(task_pf);
                taskInstance.GetComponent<TaskController>().StartTask(100f);
            }
        }

    }
}