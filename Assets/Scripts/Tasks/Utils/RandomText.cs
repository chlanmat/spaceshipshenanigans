﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.Tasks.Utils {
    public class RandomText : MonoBehaviour {

        [SerializeField] private List<string> texts;

        private void Start() {
            TextMeshPro tmp = GetComponent<TextMeshPro>();
            tmp.text = texts[Random.Range(0, texts.Count)];
        }
    }
}