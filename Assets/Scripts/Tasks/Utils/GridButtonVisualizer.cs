﻿using Assets.Scripts.HitTargets;
using Assets.Scripts.InteractiveElements.Numeric;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Utils {
    public class GridButtonVisualizer : MonoBehaviour {

        [Tooltip("Game object, that has GridInteractible and IHitTarget.")]
        [SerializeField] private InteractiveGrid grid;
        [Tooltip("Value, that this button will react to.")]
        [SerializeField] private int number;
        [SerializeField] private Sprite pressedSprite;
        [SerializeField] private Sprite releasedSprite;


        private SpriteRenderer spriteRenderer;

        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            if (spriteRenderer == null) { Debug.LogError("No SpriteRenderer found!", gameObject); }
        }

        private void Start() {
            grid.AddListenerToValueChange((value, element) => {
                if (value == number) {
                    ChangeSprite(true);
                }
            });
            grid.GetHitTarget().OnReleaseAddListener((h) => { ChangeSprite(false); });
            ChangeSprite(false);
        }

        private void ChangeSprite(bool pressed) {
            if (pressed) {
                spriteRenderer.sprite = pressedSprite;
            } else {
                spriteRenderer.sprite = releasedSprite;
            }
        }

    }
}