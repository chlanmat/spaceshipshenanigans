﻿using Assets.Scripts.InteractiveElements.Boolean;
using Assets.Scripts.InteractiveElements;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Utils {
    public class BoolVisualizer : MonoBehaviour {

        [SerializeField] private GameObject inputGO;
        [SerializeField] private Sprite pressedSprite;
        [SerializeField] private Sprite releasedSprite;

        private IValueElement<bool> input;
        private SpriteRenderer spriteRenderer;

        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            input = inputGO.GetComponent<IValueElement<bool>>();
            if (input == null) { 
                Debug.LogError("Input GO does not hace IValueElement<bool> component!", gameObject); 
            }
        }

        private void Start() {
            spriteRenderer.sprite = releasedSprite;
            input.AddListenerToValueChange(OnValueChange);
        }

        private void OnValueChange(bool pressed, IValueElement<bool> element) {
            if (pressed) {
                spriteRenderer.sprite = pressedSprite;
            } else {
                spriteRenderer.sprite = releasedSprite;
            }
        }
    }
}