﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Utils {
    public class SpriteSwitcher : MonoBehaviour {

        [SerializeField] private Sprite defaultSprite;
        [SerializeField] private Sprite changedSprite;


        private SpriteRenderer spriteRenderer;
        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = defaultSprite;
        }

        public void SetChanged(bool changed) {
            if (changed) {
                spriteRenderer.sprite = changedSprite;
            } else {
                spriteRenderer.sprite = defaultSprite;
            }
        }
    }
}