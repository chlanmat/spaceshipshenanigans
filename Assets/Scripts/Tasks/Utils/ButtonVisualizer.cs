﻿using Assets.Scripts.InteractiveElements;
using Assets.Scripts.InteractiveElements.Boolean;
using UnityEngine;

namespace Assets.Scripts.Tasks.Utils {
    public class ButtonVisualizer : MonoBehaviour {

        [SerializeField] private InteractiveButton input;
        [SerializeField] private Sprite pressedSprite;
        [SerializeField] private Sprite releasedSprite;

        private SpriteRenderer spriteRenderer;

        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Start() {
            spriteRenderer.sprite = releasedSprite;
            input.AddListenerToValueChange(OnValueChange);
        }

        private void OnValueChange(bool pressed, IValueElement<bool> element) {
            if (pressed) {
                spriteRenderer.sprite = pressedSprite;
            } else {
                spriteRenderer.sprite = releasedSprite;
            }
        } 
    }
}