﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Basketall {
    public class BallController : MonoBehaviour {

        [SerializeField] private float maxDistance = 5f;

        private Vector3 initialPosition;

        private void Start() {
            initialPosition = transform.position;
        }

        private void Update() {
            if (Vector2.Distance(initialPosition, transform.position) > maxDistance) {
                Destroy(gameObject);
            }
        }
    }
}