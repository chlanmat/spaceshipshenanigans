﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Tasks.Basketall {
    public class DunkChecker : MonoBehaviour {
        [HideInInspector] public string ballObjectName = "No name provided";
        public UnityEvent dunkEvent; 

        private void OnTriggerEnter2D(Collider2D collision) {
            if (collision.gameObject.name != ballObjectName)
                return;

            // Direction to the ball
            Vector2 dir = collision.transform.position - transform.position;
            if (dir.y > 0) {
                Debug.Log("Dunk!");
                dunkEvent.Invoke();
            }
        }

    }
}