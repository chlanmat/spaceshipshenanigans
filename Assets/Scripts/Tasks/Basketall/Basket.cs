﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Basketall {
    public class Basket : MonoBehaviour {

        [SerializeField] float timeToSlide = 2f;
        [SerializeField] float slideDistance = 2f;


        private void Start() {
            StartCoroutine(MovementCoroutine());
        }

        private IEnumerator MovementCoroutine() {
            Vector3 offset = new(slideDistance / 2, 0, 0);
            Vector3 left = transform.localPosition - offset;
            Vector3 right = transform.localPosition + offset;

            while (true) {
                yield return StartCoroutine(SlidingCoroutine(left, right, timeToSlide));
                yield return StartCoroutine(SlidingCoroutine(right, left, timeToSlide));
            }
        }

        private IEnumerator SlidingCoroutine(Vector3 from, Vector3 to, float time) {
            float startTime = Time.time;
            while (Time.time < startTime + time) {
                float portion = (Time.time - startTime) / time;
                transform.localPosition = Vector3.Lerp(from, to, portion);
                yield return null;
            }
            transform.position = to;
        }

        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.yellow;
            Vector3 offset = new(slideDistance / 2, 0, 0);
            Gizmos.DrawLine(
                transform.parent.TransformPoint(transform.localPosition - offset),
                transform.parent.TransformPoint(transform.localPosition + offset)
            );
        }
    }
}