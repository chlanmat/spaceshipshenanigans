﻿using Assets.Scripts.HitTargets;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Basketall {

    [RequireComponent(typeof(IHitTarget))]
    public class BallSpawner : MonoBehaviour {

        [SerializeField] private GameObject ballPf;
        [SerializeField] private float maxTorque = 10f;
        [SerializeField] private float maxForce = 0.5f;

        [HideInInspector] public string ballObjectName = "No name provided";

        private void Start() {
            GetComponent<IHitTarget>().OnPressAddListener(CreateBall);
        }

        private void CreateBall(Vector2 hitPos) {
            Quaternion rot = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            GameObject ballInstance = Instantiate(ballPf, hitPos, rot);
            ballInstance.transform.position = hitPos;
            ballInstance.name = ballObjectName;
            ballInstance.transform.parent = transform.parent;

            Rigidbody2D ballRB = ballInstance.GetComponent<Rigidbody2D>();
            ballRB.AddTorque(Random.Range(-maxTorque, maxTorque));
            ballRB.velocity = Random.insideUnitCircle * maxForce + new Vector2(0, maxForce/2);
        }

    }
}