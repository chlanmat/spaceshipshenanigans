﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Basketall {

    [RequireComponent(typeof(TaskController))]
    public class BasketballTask : MonoBehaviour {

        [SerializeField, Tooltip("This name will be given to instantiated balls.")] 
            private string ballObjectName = "Ball";
        [SerializeField] BallSpawner ballSpawner;
        [SerializeField] DunkChecker checker;

        TaskController controller;

        private void Start() {
            controller = GetComponent<TaskController>();
            ballSpawner.ballObjectName = ballObjectName;
            checker.ballObjectName = ballObjectName;

            checker.dunkEvent.AddListener(OnDunk);
        }

        private void OnDunk() {
            controller.EndTask(true, 0.5f);
        }

        public string GetBallObjectName() {
            return ballObjectName;
        }

    }
}