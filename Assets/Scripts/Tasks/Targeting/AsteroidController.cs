﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Targeting {
    public class AsteroidController : MonoBehaviour {

        [SerializeField] private float rotSpeed = 90;

        void Update() {
            transform.Rotate(new Vector3(0, 0, 1), rotSpeed * Time.deltaTime); 
        }
    }
}