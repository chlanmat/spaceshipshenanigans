﻿using Assets.Scripts.InteractiveElements;
using Assets.Scripts.InteractiveElements.Numeric;
using Assets.Scripts.Utils;
using UnityEngine;
using Grid = Assets.Scripts.Utils.Grid;

namespace Assets.Scripts.Tasks.Targeting {

    // Position is usually Vector3 in local space and 

    [RequireComponent (typeof(TaskController))]
    public class TargetingTask : MonoBehaviour {

        [SerializeField] private GameObject leftRightInputGO;
        [SerializeField] private GameObject upDownInputGO;
        [SerializeField] private GameObject fireButtonGO;
        [SerializeField, Tooltip("Edit the bounds of area. Rows and cols will be overwritten.")] 
            private Grid targetingArea;
        [SerializeField] private GameObject objectpf;
        [SerializeField] private Transform cross;
        [SerializeField] private GameObject fireMarkerPf;

        // Position of the targeted object inside grid.
        private Vector2Int objectCoords;
        private GameObject objectinstance;

        private INumericElement<int> leftRightInput;
        private INumericElement<int> upDownInput;
        private IValueElement<bool> fireButton;
        private TaskController controller;


        private void Awake() {
            leftRightInput = leftRightInputGO.GetComponent<INumericElement<int>>();
            upDownInput = upDownInputGO.GetComponent<INumericElement<int>>();
            fireButton = fireButtonGO.GetComponent<IValueElement<bool>>();

            leftRightInput.AddListenerToValueChange(UpdateCrossPosition);
            upDownInput.AddListenerToValueChange(UpdateCrossPosition);
            fireButton.AddListenerToValueChange(FireButtonValChange);

            controller = GetComponent<TaskController>();
        }

        private void Start() {
            targetingArea.cols = leftRightInput.GetValueBounds().max;
            targetingArea.rows = upDownInput.GetValueBounds().max;
            SetupObject();
            UpdateCrossPosition(0, null);
        }

        private void FireButtonValChange(bool value, IValueElement<bool> input) { 
            if (value) {
                Fire();
            }
        }

        private void Fire() {
            Instantiate(fireMarkerPf, transform).transform.localPosition = GetPosFromCoords(GetInputValues());


            if (GetInputValues() == objectCoords) {
                Destroy(objectinstance);
                controller.EndTask(true, 0.5f);
            }
        }

        private void UpdateCrossPosition(int value, IValueElement<int> element) {
            Vector2Int inputValues = GetInputValues();
            cross.localPosition = GetPosFromCoords(inputValues);
        }

        private Vector2Int GetInputValues() {
            return new Vector2Int(leftRightInput.GetValue(), upDownInput.GetValue());
        }

        private void SetupObject() {
            objectCoords = GetRandomCoords(
                new(targetingArea.cols + 1, targetingArea.rows + 1),
                new(leftRightInput.GetValue(), upDownInput.GetValue())
            );

            objectinstance = Instantiate(objectpf, Vector3.zero, Quaternion.identity, transform);
            objectinstance.transform.localPosition = GetPosFromCoords(objectCoords);
        }

        private Vector2 GetPosFromCoords(Vector2Int coords) {
            return new Vector2(
                    targetingArea.GetVerticalBorder(coords.x),
                    targetingArea.GetHorizontalBorder(coords.y)
                );
        }

        private Vector2Int GetRandomCoords(Vector2Int maxExclusive, Vector2Int excluding) {
            Vector2Int result = new() {
                x = Random.Range(0, maxExclusive.x - 1),
                y = Random.Range(0, maxExclusive.y - 1)
            };

            if (result.x >= excluding.x)
                result.x++;
            if (result.y >= excluding.y)
                result.y++;

            return result;
        }


        private void OnDrawGizmosSelected() {
            Gizmos.color = Color.white;
            foreach (var line in targetingArea.GetLines()) {
                Gizmos.DrawLine(transform.TransformPoint(line.Item1), transform.TransformPoint(line.Item2));
            }
        }

    }
}