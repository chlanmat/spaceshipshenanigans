﻿using System.Collections;
using Unity.VisualScripting;
using UnityEngine;

namespace Assets.Scripts.Tasks {
    public class PlayAreaSettings {

        public FloatPrefsValue leftPadding = new("leftPadding", 0.03f, 0f, 0.5f);
        public FloatPrefsValue rightPadding = new("rightPadding", 0.03f, 0f, 0.5f);
        public FloatPrefsValue topPadding = new("topPadding", 0.07f, 0f, 0.5f);
        public FloatPrefsValue bottomPadding = new("bottomPadding", 0.06f, 0f, 0.5f);
        public IntPrefsValue touchRows = new("touchRows", 2, 0, 999);
        public FloatPrefsValue cameraSize = new("cameraSize", 5f, 5f, 20f);

        public void Save() {
            PlayerPrefs.Save();
        }

        public class FloatPrefsValue {
            private string name;
            private float min;
            private float max;

            public FloatPrefsValue(string name, float defaultValue, float min, float max) {
                this.name = name;
                this.min = min;
                this.max = max;

                if (!PlayerPrefs.HasKey(name)) {
                    Set(defaultValue);
                }
            }

            public void Set(float value) {
                PlayerPrefs.SetFloat(name, Mathf.Clamp(value, min, max));
            }

            public float Get() {
                return PlayerPrefs.GetFloat(name);
            }

            public (float min, float max) GetRange() {
                return (min, max);
            }
        }

        public class IntPrefsValue {
            private string name;
            private int min;
            private int max;

            public IntPrefsValue(string name, int defaultValue, int min, int max) {
                this.name = name;
                this.min = min;
                this.max = max;

                if (!PlayerPrefs.HasKey(name)) {
                    Set(defaultValue);
                }
            }

            public void Set(int value) {
                PlayerPrefs.SetInt(name, Mathf.Clamp(value, min, max));
            }

            public int Get() {
                return PlayerPrefs.GetInt(name);
            }

            public (int min, int max) GetRange() {
                return (min, max);
            }
        }

    }
}