﻿using Assets.Scripts.Tasks.Nonspecific;
using Assets.Scripts.Tasks.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Tasks.Decoding {

    public class ProgressDisplay : MonoBehaviour {

        [SerializeField] private SequentialCodeTask task;
        [SerializeField] private List<SpriteSwitcher> diodes;

        private int correctCount = 0;

        private void Start() {
            task.correctChoiceEvent.AddListener(CorrectChoice);
            task.wrongChoiceEvent.AddListener(WrongChoice);
        }

        private void CorrectChoice(int value) {
            diodes[correctCount].SetChanged(true);
            correctCount++;
        }

        private void WrongChoice() {
            foreach (var diode in diodes) {
                diode.SetChanged(false);
            }
            correctCount = 0;
        }
    }
}