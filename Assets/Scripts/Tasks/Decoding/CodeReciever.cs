﻿using Assets.Scripts.Tasks.Nonspecific;
using Assets.Scripts.Tasks.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Tasks.Decoding {
    public class CodeReciever : MonoBehaviour {

        [SerializeField] private SequentialCodeTask task;
        [SerializeField] private float lightLength = 1f;
        [SerializeField] private float lightPauseLength = 0.1f;
        [SerializeField] private float codePauseLength = 2f;
        [SerializeField] private List<SpriteSwitcher> lights;

        List<int> code;

        private void Start() {
            code = task.GetCode();
            StartCoroutine(BlinkingCoroutine());
        }

        private IEnumerator BlinkingCoroutine() { 
            while (true) {
                yield return new WaitForSeconds(codePauseLength);
                foreach (int n in code) {
                    lights[n].SetChanged(true);
                    yield return new WaitForSeconds(lightLength);
                    lights[n].SetChanged(false);
                    yield return new WaitForSeconds(lightPauseLength);
                }
            }
        }
    }
}