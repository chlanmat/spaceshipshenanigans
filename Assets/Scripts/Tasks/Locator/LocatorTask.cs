﻿using Assets.Scripts.InteractiveElements;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Tasks.Locator {

    [RequireComponent(typeof(TaskController))]
    public class LocatorTask : MonoBehaviour {

        [SerializeField] private Sprite planetSprite;
        [SerializeField] private List<Sprite> emptySprites;
        [SerializeField] private GameObject nextScanButtonGO;
        [SerializeField] private GameObject confirmButtonGO;
        [SerializeField, Tooltip("Minimum number of scans, including first and planet scan.")] 
            private int minScans = 3;
        [SerializeField, Tooltip("Maximum number of scans, including first and planet scan.")] 
            private int maxScans = 6;
        [SerializeField] private SpriteRenderer display;



        private IValueElement<bool> nextScanButton;
        private IValueElement<bool> confirmButton;

        private List<Sprite> scanQueue = new();
        private int currentScan = 0;

        private void Awake() {
            SetupScanQueue();
        }

        private void Start() {
            nextScanButton = nextScanButtonGO.GetComponent<IValueElement<bool>>();
            confirmButton = confirmButtonGO.GetComponent<IValueElement<bool>>();

            nextScanButton.AddListenerToValueChange(NewScan);
            confirmButton.AddListenerToValueChange(ConfirmScan);

            display.sprite = scanQueue[0];
        }

        private void NewScan(bool value, IValueElement<bool> element) {
            if (value == false) {
                return;
            }

            currentScan++;
            if (currentScan >= scanQueue.Count) {
                currentScan = 0;
            }
            display.sprite = scanQueue[currentScan];
        }

        private void ConfirmScan(bool value, IValueElement<bool> element) {
            if (value == false) {
                return;
            }
            if (currentScan != scanQueue.Count - 1) {
                return;
            }

            GetComponent<TaskController>().EndTask(true, 0.5f);

        }



        private void SetupScanQueue() {
            if (maxScans - 1 > emptySprites.Count) {
                Debug.Log($"Not enough empty sprites. Needed {maxScans - 1}, provided {emptySprites.Count})");
            }

            int scanCount = Random.Range(minScans, maxScans + 1);

            // Setup empty scans.
            for (int i = 0; i < scanCount - 1; i++) {
                int chosenPos = Random.Range(0, emptySprites.Count);
                scanQueue.Add(emptySprites[chosenPos]);
                emptySprites.RemoveAt(chosenPos);
            }

            // Add planet scan.
            scanQueue.Add(planetSprite);
        }
    }
}