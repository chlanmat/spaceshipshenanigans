﻿using Assets.Scripts.InteractiveElements;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Tasks.Nonspecific {
    [RequireComponent(typeof(TaskController))]
    public class PressTask : MonoBehaviour {

        [SerializeField] private GameObject inputGO;



        private IValueElement<bool> input;
        private TaskController task;


        private void Awake() {
            task = GetComponent<TaskController>();
        }

        private void Start() {
            input = inputGO.GetComponent<IValueElement<bool>>();
            input.AddListenerToValueChange(OnValueChange);
        }

        private void OnValueChange(bool value, IValueElement<bool> element) {
            if (value == true) {
                task.EndTask(true, 0.5f);
            }
        }

        private void OnValidate() {
            if (inputGO != null && inputGO.GetComponent<IValueElement<bool>>() == null) {
                inputGO = null;
                Debug.LogWarning("Can't assign game object that doesn't have" +
                    " any component of type IValueElement<int>.", gameObject);
            }
        }
    }
}