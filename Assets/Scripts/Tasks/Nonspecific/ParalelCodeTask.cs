﻿using Assets.Scripts.InteractiveElements;
using Assets.Scripts.InteractiveElements.Numeric;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Tasks.Nonspecific {

    /// <summary>
    /// The goal of this task is to set all input elements to correct value.
    /// </summary>
    [RequireComponent(typeof(TaskController))]
    public class ParalelCodeTask : MonoBehaviour {

        [Tooltip("List of interactive elements. Must be ordered left to right.")]
        [SerializeField] private List<GameObject> inputsGO;
        [SerializeField] private bool excludeInitialPositions = true;
        [SerializeField] private float destroyDelay = 0.5f;

        public UnityEvent<List<bool>> onInputChange;


        private List<INumericElement<int>> inputs = new();
        private List<int> code = new();
        private TaskController task;


        public List<int> GetCode() { return code; }

        private void Awake() {
            task = GetComponent<TaskController>();
        }

        private void Start() {
            // Create list of inputs
            for (int i = 0; i < inputsGO.Count; i++) {
                inputs.Add(inputsGO[i].GetComponent<INumericElement<int>>());
            }

            foreach (var input in inputs) {
                input.AddListenerToValueChange(OnAnyValueChange);
            }

            GenerateCode();
            // Debug.Log("The code is " + string.Concat(code));
            OnAnyValueChange(0, null);
        }


        private void OnAnyValueChange(int val, IValueElement<int> element) {
            bool isCorrect = true;
            List<bool> correctPositions = new();

            for (int i = 0; i < inputs.Count; i++) {
                if (inputs[i].GetValue() == code[i]) {
                    correctPositions.Add(true);
                } else {
                    correctPositions.Add(false);
                    isCorrect = false;
                }
            }

            onInputChange.Invoke(correctPositions);

            if (isCorrect && !task.HasEnded()) {
                StopTask();
            }
        }

        private void StopTask() {
            task.EndTask(true, destroyDelay);

            foreach (var input in inputs) {
                input.GetHitTarget().SetInteractive(false);
            }
        }

        private void GenerateCode() {
            foreach (var input in inputs) {
                (int min, int max) = input.GetValueBounds();
                int val;
                if (excludeInitialPositions) {
                    val = Random.Range(min, max); // Generate one less value than input value count.
                    if (val >= input.GetValue()) // Skip the initial value if necesarry.
                        val++;
                } else {
                    // Input bounds are inclusive, but Randoom.Range max is exclusive. So max + 1.
                    val = Random.Range(min, max + 1);
                }
                code.Add(val);
            }
        }

        private void OnValidate() {
            for (int i = 0; i < inputsGO.Count; i++) {
                if (inputsGO[i] == null) {
                    continue;
                }
                if (inputsGO[i].GetComponent<INumericElement<int>>() == null) {
                    Debug.LogWarning("Can't add game object that doesnt have INumericElement<int>!", gameObject);
                    inputsGO[i] = null;
                }
            }
        }
    }
}