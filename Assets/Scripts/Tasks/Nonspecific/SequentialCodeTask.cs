﻿using Assets.Scripts.InteractiveElements;
using Assets.Scripts.InteractiveElements.Numeric;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Tasks.Nonspecific {

    /// <summary>
    /// The goal of this task is to make the correct sequence
    /// on one input element.
    /// </summary>

    [RequireComponent(typeof(TaskController))]
    public class SequentialCodeTask : MonoBehaviour {

        [SerializeField] private int codeLength = 4;
        [SerializeField] private GameObject inputGO;
        private INumericElement<int> input;


        public UnityEvent<int> correctChoiceEvent;
        public UnityEvent wrongChoiceEvent;

        private List<int> code = new();
        // Position of symbol that needs to be pressed next.
        private int codePos = 0;
        private TaskController task;



        public List<int> GetCode() { return code; }

        public (int min, int max) GetCodeBounds() {
            return input.GetValueBounds();
        }

        private void Awake() {
            task = GetComponent<TaskController>();
        }

        private void Start() {
            if (inputGO == null) { Debug.LogError($"inputGO cannot be null!"); return; }
            input = inputGO.GetComponent<INumericElement<int>>();
            GenerateCode();
            // Debug.Log("The code is: " + string.Concat(code), gameObject);

            input.AddListenerToValueChange(OnValueChange);
        }

        private void OnValueChange(int value, IValueElement<int> element) {
            if (code[codePos] != value) {
                wrongChoiceEvent.Invoke();
                codePos = 0;
                return;
            }

            correctChoiceEvent.Invoke(value);
            codePos++;
            if (codePos == code.Count) {
                // Full correct code inputted.
                StopTask();
            }

        }

        private void StopTask() {
            input.GetHitTarget().SetInteractive(false);
            task.EndTask(true, 0.5f);
        }

        private void GenerateCode() {
            var (min, max) = input.GetValueBounds();

            for (int i = 0; i < codeLength; i++) {
                code.Add(Random.Range(min, max + 1));
            }
        }

        private void OnValidate() {
            if (inputGO != null && inputGO.GetComponent<IValueElement<int>>() == null) {
                inputGO = null;
                Debug.LogWarning("Can't assign game object that doesn't have" +
                    " any component of type IValueElement<int>.", gameObject);
            }
        }
    }
}