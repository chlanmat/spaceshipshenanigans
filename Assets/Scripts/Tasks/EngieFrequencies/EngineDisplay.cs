﻿using Assets.Scripts.Tasks.Nonspecific;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Tasks.EngieFrequencies {
    public class EngineDisplay : MonoBehaviour {

        [SerializeField] private List<Sprite> red;
        [SerializeField] private List<Sprite> green;
        [SerializeField] private ParalelCodeTask task;

        private SpriteSequence spriteSequence;
        private bool isGreen = false;

        private void Awake() {
            spriteSequence = GetComponent<SpriteSequence>();
        }

        private void Start() {
            task.onInputChange.AddListener(OnValueUpdate);
            spriteSequence.SetSprites(red);
        }

        private void OnValueUpdate(List<bool> correctPositions) {
            bool isCorrect = FullyCorrect(correctPositions);
            if (isCorrect == isGreen)
                return;

            isGreen = isCorrect;

            if (isGreen) {
                spriteSequence.SetSprites(green);
            } else {
                spriteSequence.SetSprites(red);
            }
        }

        private bool FullyCorrect(List<bool> correctPositions) {
            return !correctPositions.Contains(false);
        }
    }
}