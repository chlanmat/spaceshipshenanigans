﻿using Assets.Scripts.Tasks.Nonspecific;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Tasks.EngieFrequencies {
    public class FrequencyDisplay : MonoBehaviour {

        [SerializeField] private List<Sprite> red;
        [SerializeField] private List<Sprite> green;
        [SerializeField] private int inputNumber;
        [SerializeField] private ParalelCodeTask task;

        private SpriteSequence spriteSequence;
        private bool isGreen = false;

        private void Awake() {
            spriteSequence = GetComponent<SpriteSequence>();
        }

        private void Start() {
            task.onInputChange.AddListener(OnValueUpdate);
            spriteSequence.SetSprites(red);
        }

        private void OnValueUpdate(List<bool> correctPositions) {
            if (correctPositions[inputNumber] == isGreen)
                return;

            isGreen = correctPositions[inputNumber];

            if (isGreen) {
                spriteSequence.SetSprites(green);
            } else {
                spriteSequence.SetSprites(red);
            }
        }
    }
}