﻿using Assets.Scripts.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Grid = Assets.Scripts.Utils.Grid;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Tasks {
    public class TaskSpawner : MonoBehaviour {

        [SerializeField] private List<GameObject> touchTask_pfs;
        [SerializeField] private List<GameObject> throwTask_pfs;
        [SerializeField] private int players = 4;
        [SerializeField] private float tasksPerPlayer = 1.5f;
        [SerializeField, Range(0, 1f)] private float throwTouchRatio = 2 / 3f;
        [SerializeField] private float minTaskDelay;
        [SerializeField] private float maxTaskDelay;
        [SerializeField] private bool continueAfterEnd = false;

        private PlayAreaSettings playArea;

        private Grid taskPositions;
        private GameObject[,] tasks;
        private GameManager gameManager;

        private int maxTouchTasks;
        private int maxThrowTasks;

        private void Start() {
            playArea = new();
            taskPositions = MakeGrid();
            tasks = new GameObject[taskPositions.cols, taskPositions.rows];
            gameManager = GameManager.GetInstance();
            gameManager.gameEndedEvent.AddListener(OnGameEnd);
            ComputeMaxTasks();

            StartCoroutine(SpawningLoop(GetEmptyTouching, touchTask_pfs, TouchSlots(), maxTouchTasks));
            StartCoroutine(SpawningLoop(GetEmptyThrowing, throwTask_pfs, ThrowSlots(), maxThrowTasks));
        }

        private IEnumerator SpawningLoop(
            Func<List<(int col, int row)>> getEmptyCells, 
            List<GameObject> taskPool,
            int totalSlots,
            int maxTaskCount
        ) {
            while (true) {
                yield return new WaitForSeconds(Random.Range(minTaskDelay, maxTaskDelay));

                var availableCells = getEmptyCells();
                // Debug.Log("Available count: " + available.Count);
                int currentTaskCount = totalSlots - availableCells.Count;
                if (currentTaskCount >= maxTaskCount) {
                    continue;
                }

                (int col, int row) = availableCells[Random.Range(0, availableCells.Count)];

                GameObject newTask = Instantiate(
                    taskPool[Random.Range(0, taskPool.Count)],
                    taskPositions.GetCellCenter(col, row),
                    Quaternion.identity
                );
                float taskTimeMultiplier = gameManager.GetDangerLevel().taskTimeMultiplier;
                newTask.GetComponent<TaskController>().StartTask(taskTimeMultiplier);

                tasks[col, row] = newTask;

            }
        }

        private void OnGameEnd() {
            if (continueAfterEnd) {
                return;
            }

            StopAllCoroutines();
            foreach (GameObject go in tasks) {
                if (go != null) {
                    Destroy(go);
                }
            }
        }

        private List<(int col, int row)> GetEmptyTouching() {
            List<(int col, int row)> emptyCoords = new();

            for (int row = 0; row < playArea.touchRows.Get(); row++) {
                for (int col = 0; col < taskPositions.cols; col++) {
                    if (tasks[col, row] == null) {
                        emptyCoords.Add((col, row));
                    }
                }
            }

            return emptyCoords;
        }

        private List<(int col, int row)> GetEmptyThrowing() {
            List<(int col, int row)> emptyCoords = new();

            for (int row = playArea.touchRows.Get(); row < taskPositions.rows; row++) {
                for (int col = 0; col < taskPositions.cols; col++) {
                    if (tasks[col, row] == null) {
                        emptyCoords.Add((col, row));
                    }
                }
            }

            return emptyCoords;
        }


        private Grid MakeGrid() {
            Vector2 botLeft = CameraController.Instance.GetDefaultBotLeft();
            Vector2 topRight = CameraController.Instance.GetDefaultTopRight();

            float top = Mathf.Lerp(topRight.y, botLeft.y, playArea.topPadding.Get());
            float right = Mathf.Lerp(topRight.x, botLeft.x, playArea.rightPadding.Get());
            float bottom = Mathf.Lerp(botLeft.y, topRight.y, playArea.bottomPadding.Get());
            float left = Mathf.Lerp(botLeft.x, topRight.x, playArea.leftPadding.Get());

            return new Grid(
                top,
                right,
                bottom,
                left,
                Mathf.FloorToInt((right - left) / TaskController.maxTaskSize.x),
                Mathf.FloorToInt((top - bottom) / TaskController.maxTaskSize.y)
            );
        }

        /// <summary>
        /// Computes the maxThrowTasks and maxTouchTasks values.
        /// </summary>
        private void ComputeMaxTasks() {
            // Touch tasks limit. There always should be at least one task slot empty.
            int touchCountLimit = TouchSlots() - 1;
            // Throw tasks limit. There always should be at least one task slot empty.
            int throwCountLimit = ThrowSlots() - 1;
            // How many total tasks there should be.
            int taskCountTarget = Mathf.CeilToInt(players * tasksPerPlayer);
            // How many throw tasks there should be.
            int throwTarget = Mathf.CeilToInt(throwTouchRatio * taskCountTarget);
            // throwTarget is the desired value capped from top by touch count limit
            maxThrowTasks = Mathf.Min(throwTarget, throwCountLimit);
            // The rest of the tasks should be touch
            int touchTarget = taskCountTarget - maxThrowTasks;
            maxTouchTasks = Mathf.Min(touchTarget, touchCountLimit);

            // If the throwTouchRatio is too small (close to zero) then not
            // all tasks will be assigned. This results in not enough throw tasks
            // while touch tasks are full. So if there are some remaining, make
            // them throw tasks.
            int remainingTasks = taskCountTarget - (maxThrowTasks + maxTouchTasks);
            if (remainingTasks > 0) {
                maxThrowTasks = Mathf.Min(maxThrowTasks + remainingTasks, throwCountLimit);
            }

            Debug.Log($"maxTouchTasks: {maxTouchTasks}, maxThrowTasks: {maxThrowTasks}", gameObject);
        } 

        private int TouchSlots() {
            return taskPositions.cols * playArea.touchRows.Get();
        }

        private int ThrowSlots() {
            return taskPositions.cols * (taskPositions.rows - playArea.touchRows.Get());
        }

        private void OnDrawGizmos() {
            //Gizmos.color = Color.white;
            //Grid rect = MakeGrid();

            //foreach (var line in rect.GetLines()) {
            //    Gizmos.DrawLine(line.Item1, line.Item2);
            //}
        }
    }
}
