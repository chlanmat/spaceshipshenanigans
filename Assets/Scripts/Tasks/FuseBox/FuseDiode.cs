﻿using Assets.Scripts.Tasks.Nonspecific;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Tasks.FuseBox {
    public class FuseDiode : MonoBehaviour {

        [SerializeField] private Sprite red;
        [SerializeField] private Sprite green;
        [SerializeField] private int inputNumber;
        [SerializeField] private ParalelCodeTask task;

        private SpriteRenderer spriteRenderer;

        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Start() {
            task.onInputChange.AddListener(OnValueUpdate);
        }

        private void OnValueUpdate(List<bool> correctPositions) {
            if (correctPositions[inputNumber] == true) {
                spriteRenderer.sprite = green;
            } else {
                spriteRenderer.sprite = red;
            }
        }
    }
}