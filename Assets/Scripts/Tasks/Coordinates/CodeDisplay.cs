﻿using Assets.Scripts.Tasks.Nonspecific;
using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

namespace Assets.Scripts.Tasks.Coordinates {

    public class CodeDisplay : MonoBehaviour {

        [SerializeField] private SequentialCodeTask task;
        [SerializeField] private SpriteLine correctCode;
        [SerializeField] private SpriteLine inputCode;
        [SerializeField] private List<Sprite> symbols;

        private void Start() {
            InitialChecks();
            task.correctChoiceEvent.AddListener(CorrectChoice);
            task.wrongChoiceEvent.AddListener(WrongChoice);

            foreach (int num in task.GetCode()) {
                correctCode.Add(symbols[num]);
            }
        }

        private void CorrectChoice(int value) {
            inputCode.Add(symbols[value]);
        }

        private void WrongChoice() {
            inputCode.Clear();
        }


        private void InitialChecks() {
            if (task.GetCodeBounds() != (0, symbols.Count-1)) {
                // Input may not start at 0 or has wrong number of options.
                Debug.LogError("Number of symbols must be the same as input options!", gameObject);
            }
        }
    }
}