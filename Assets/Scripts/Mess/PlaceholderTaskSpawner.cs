using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceholderTaskSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> task_pfs;

    private int index = 0;
    private GameObject taskInstance;
    private void Update() {
        if (taskInstance == null) {
            taskInstance = Instantiate(task_pfs[index], transform.position, Quaternion.identity);
            index++;
            if (index >= task_pfs.Count) {
                index = 0;
            }
        }
    }
}
