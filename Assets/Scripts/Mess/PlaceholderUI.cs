﻿using Assets.Scripts.Utils;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
///     Just temporary UI script. REMOVE AT LATER DATE!!!
/// </summary>
public class PlaceholderUI : MonoBehaviour
{
    [SerializeField] private TextMeshPro scoreText;
    [SerializeField] private TextMeshPro livesText;
    [SerializeField] private TextMeshPro dangerText;

    GameManager gameManager;

    private void Start() {
        gameManager = GameManager.GetInstance();
        gameManager.livesChangeEvent.AddListener(ChangeLives);
        gameManager.scoreChangeEvent.AddListener(ChangeScore);
        gameManager.dangerLevelChangeEvent.AddListener(ChangeDanger);

    }

    public void ChangeScore(int value) {
        scoreText.text = "Skóre: " + value;
    }
    public void ChangeLives(int current, int max) {
        livesText.text = $"Životy: {current}/{max}";
    }

    public void ChangeDanger(DangerLevel dl) {
        dangerText.text = $"Nebezpečí: {dl.level}, násobič: {dl.scoreMultiplier}x";
    }
}
