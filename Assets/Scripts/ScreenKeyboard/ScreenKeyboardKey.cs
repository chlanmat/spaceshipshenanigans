using Assets.Scripts.HitTargets;
using UnityEngine;
using UnityEngine.Events;

public class ScreenKeyboardKey : MonoBehaviour {

    SpriteRenderer spriteRenderer;
    [SerializeField]
    char character;
    [SerializeField]
    UnityEvent<char> keyPress;

    IHitTarget hitTarget;

    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        hitTarget = GetComponent<IHitTarget>();
        hitTarget.OnPressAddListener(OnPress);
        hitTarget.OnReleaseAddListener(OnRelease);
    }

    public void OnPress(Vector2 hitPosition) {
        spriteRenderer.color = Color.gray;
        keyPress.Invoke(character);
    }

    public void OnRelease(Vector2 hitPosition) {
        spriteRenderer.color = Color.white;
    }
}
