﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI {
    public class UiQuit : MonoBehaviour {

        void Start() {
            GetComponent<IUiActionElement>().AddListenerToAction(OnAction);
        }

        private void OnAction() {
            Application.Quit();
        }
    }
}