﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.UI {
    public interface IUiActionElement {

        public void AddListenerToAction(UnityAction call);
        public void RemoveListenerFromAction(UnityAction call);
    }
}