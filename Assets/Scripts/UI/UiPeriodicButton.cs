﻿using Assets.Scripts.HitTargets;
using initi.input;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.UI {


    /// <summary>
    /// This button sends signal while held in set intervals.
    /// </summary>
    public class UiPeriodicButton : MonoBehaviour, IUiActionElement {

        [SerializeField] private float actionPeriod = 0.5f;

        private UnityEvent actionEvent = new();
        private IHitTarget hitListener;
        private Coroutine periodicInvoke;

        // Use this for initialization
        void Awake() {
            hitListener = GetComponent<IHitTarget>();
            hitListener.OnPressAddListener(OnPress);
            hitListener.OnReleaseAddListener(OnRelease);
        }

        private void OnPress(Vector2 hitPosition) {
            periodicInvoke = StartCoroutine(InvokeCoroutine());
        }

        private void OnRelease(Vector2 hitPosition) { 
            StopCoroutine(periodicInvoke);
        }

        private IEnumerator InvokeCoroutine() {
            while (true) {
                actionEvent.Invoke();
                yield return new WaitForSeconds(actionPeriod);
            }
        }

        public void AddListenerToAction(UnityAction call) {
            actionEvent.AddListener(call);
        }

        public void RemoveListenerFromAction(UnityAction call) {
            actionEvent.RemoveListener(call);
        }

    }
}