﻿using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.UI {
    public class UiContainer : MonoBehaviour {

        [Tooltip("If this container should be attached to top or bottom")]
        [SerializeField] bool fromBottom = true;

        private float distance;

        private void Start() {
            if (fromBottom) {
                distance = transform.position.y - CameraController.Instance.GetDefaultBotLeft().y;
            } else {
                distance = CameraController.Instance.GetDefaultTopRight().y - transform.position.y;
            }

        }

        void Update() {
            if (fromBottom) {
                Vector2 botLeft = CameraController.Instance.GetCurrentBotLeft();
                Vector3 newPos = transform.position;
                newPos.y = botLeft.y + distance;
                transform.position = newPos;
            } else {
                Vector2 topRight = CameraController.Instance.GetCurrentTopRight();
                Vector3 newPos = transform.position;
                newPos.y = topRight.y - distance;
                transform.position = newPos;
            }

        }
    }
}