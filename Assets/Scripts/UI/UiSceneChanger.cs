﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.UI {
    public class UiSceneChanger : MonoBehaviour {

        [SerializeField] private string sceneName;

        void Start() {
            GetComponent<IUiActionElement>().AddListenerToAction(OnAction);
        }

        private void OnAction() {
            SceneManager.LoadScene(sceneName);
        }
    }
}