using Assets.Scripts.InteractiveElements;
using Assets.Scripts.InteractiveElements.Numeric;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.UI {
    public class UiHoldButton : MonoBehaviour, IUiActionElement {

        [SerializeField] private float holdTime = 2f;
        [SerializeField] private SpriteRenderer background;
        [SerializeField] private SpriteRenderer progressDisplay;

        private UnityEvent actionEvent = new();
        private float lastValue = 0f;
        private SpriteRenderer spriteRenderer;
        private INumericElement<float> interactiveButton;

        private void Awake() {
            spriteRenderer = GetComponent<SpriteRenderer>();
            interactiveButton = GetComponent<INumericElement<float>>();
        }

        private void Start() {
            interactiveButton.AddListenerToValueChange(OnValueChange);
            AlignBackground();
        }



        public void SetButtonInteractive(bool interactive) {
            interactiveButton.GetHitTarget().SetInteractive(interactive);
        }



        private void OnValueChange(float value, IValueElement<float> element) {
            if (lastValue < holdTime && value >= holdTime) {
                actionEvent.Invoke();
            }
            lastValue = value;
            SetProgressDisplay(value / holdTime);
        }

        private void SetProgressDisplay(float t) {
            t = Mathf.Clamp01(t);
            progressDisplay.size = new(spriteRenderer.size.x * t, spriteRenderer.size.y);
        }

        [ContextMenu("Align background")]
        private void AlignBackground() {
            background.transform.localPosition = Vector3.zero;
            progressDisplay.transform.localPosition = Vector3.zero;
            // This condidion is here because this method can be called from editor.
            if (spriteRenderer == null) {
                spriteRenderer = GetComponent<SpriteRenderer>();
            }
            background.size = spriteRenderer.size;
        }

        public void AddListenerToAction(UnityAction call) {
            actionEvent.AddListener(call);
        }

        public void RemoveListenerFromAction(UnityAction call) {
            actionEvent.RemoveListener(call);
        }
    }
}